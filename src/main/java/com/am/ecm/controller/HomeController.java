package com.am.ecm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
	@RequestMapping("/home")
	public String hello(HttpServletRequest request, HttpServletResponse reponses, Model model) {

		model.addAttribute("greeting", "Hello Spring MVC");

		return "home";
	}

	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	public String redirect(HttpServletRequest request, HttpServletResponse reponses, Model model) {
		return "redirect:/home";
	}

	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public String algumaInfo(Model model, @RequestParam(value = "nome", defaultValue = "Guest") String name) {
		model.addAttribute("nome", name);
		return "info";
	}

	@RequestMapping("/usuario/{nome}")
	public String paginaDoUsuario(Model model, @PathVariable(value = "nome") String nome) {
		model.addAttribute("nome", nome);
		return "usuario";
	}

	@RequestMapping("/usuario/{nome}/galeria/{idGaleria}")
	public String galeriaDoUsuario(Model model, @PathVariable(value = "nome") String nome,
			@PathVariable(value = "idGaleria") String idGaleria) {
		model.addAttribute("nome", nome);
		model.addAttribute("idGaleria", idGaleria);
		return "galeria";
	}

	// Simple example, method returns String.
	@RequestMapping(value = "/save")
	@ResponseBody
	public String save(Model model) {
		return "SALVO!!! CARAI";
	}
}
